====
DXSP
====

.. image:: ../docs/_static/logo-full.png
  :width: 200
  :alt: logo
  :align: right

| Swap made easy
| Trade on any blockchains with uniswap based router or 0x protocol.


User Guide
==========

.. toctree::
   :maxdepth: 4

   01_start
   02_config


Module Reference
================

.. toctree::
   :maxdepth: 4

   03_module


.. raw:: html

   <br>
